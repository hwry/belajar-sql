Tugas Harian 10

Jawaban:

1.  Membuat database
    create database myshop

2.  Membuat Tabel
    a). Users
        create table users(
        -> id int(8) primary key auto_increment,
        -> name varchar(255),
        -> email varchar(255),
        -> password varchar(255)
        ->);

    b). categories
        create table categories(
        -> id int(8) primary key auto_increment,
        -> name varchar(255)
        ->);

    c). items
        create table items(
        -> id int(8) primary key auto_increment,
        -> name varchar(255),
        -> description varchar(255),
        -> price int,
        -> stock int,
        -> category_id int,
        -> foreign key (category_id) references category(id),
        ->);

3. Memasukkan data ke Tabel
    a). Users
        - INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

    b). Categories
        - INSERT INTO categories (name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

    c). Items
        - INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang juur banget", 2000000, 10, 1);

4. Mengambil dara dari database
    
    a). Mengambil Data Users
        - SELECT * FROM users;

    b). Mengambil Data Items
        - SELECT * FROM item WHERE price > 1000000;
        - SELECT * FROM item WHERE name LIKE 'uniklo';

    c). Menampilkan Data Items Join Dengan Kategori
        - SELECT items.name, items.description, items.price, items.stock, categories,nama from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari database

    - Update items set price=2500000 where id=1;